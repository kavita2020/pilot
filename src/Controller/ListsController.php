<?php

namespace App\Controller;

use App\Entity\Lists;
use App\Entity\ListItem;
use App\Form\ListsType;
use App\Form\ListItemType;
use App\Repository\ListsRepository;
use App\Repository\ListItemRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class ListsController extends AbstractController
{
    private $listsRepository;
    private $listItemRepository;
    private $security;

    public function __construct(ListsRepository $listsRepository, Security $security, ListItemRepository $listItemRepository)
    {
        $this->listsRepository = $listsRepository;
        $this->security = $security;
        $this->listItemRepository = $listItemRepository;
    }

    /* redirect after login and fetch lists with list items*/
    public function index()
    {

        $form = $this->listForm();
        $itemform = $this->listItemForm();
        $user = $this->security->getUser();
        $user_id = $user->getId();

        if ($user->getFirstTimeLogin() == true && $user->isVerified() == false ){
            return $this->redirectToRoute("app_login");                    
        }

        /* get list and render index view */
        $lists = $this->listsRepository->findBy(['user'=>$user_id], ['id'=>'DESC']);

        return $this->render('lists/index.html.twig', [
            'lists' => $lists,
            'form' => $form->createView(),
            'itemform' => $itemform->createView()
        ]);
    }

    /* call it to make form to add list */
    public function listForm()
    {
        $list = new Lists();
        $form = $this->createForm(ListsType::class, $list);
        return $form;   
    }

    /* call it to make form to add items in list */
    public function listItemForm()
    {
        $item = new ListItem();
        $form = $this->createForm(ListItemType::class, $item);
        return $form;   
    }

    /* add new list into database */
    public function new(Request $request): JsonResponse
    {
        $data = $request->request->all();
        $form = $this->listForm();
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            /* call repository function to create new list into database  */
            $add = $this->listsRepository->create($form);
            
            if ($add == "false"){
                $result['status'] = "fail";
            }else{
                $result['status'] = "success";
                $result['list']['id'] = $add->getId();
                $result['list']['encode_id'] = base64_encode($add->getId());
                $result['list']['name'] = $add->getName();
                $result['list']['url'] = $this->generateUrl("lists_delete", ['id'=>base64_encode($add->getId())] );
            }

            return new JsonResponse($result);
        }
    }

    /* get detail of list on the basis of list id*/
    public function detail($id)
    {
        $id = base64_decode($id);
        
        $result['status'] = "fail";
        $list = $this->listsRepository->find($id);
        if($list){
            $result['status'] = "success";
            $result['id'] = base64_encode($list->getId());
            $result['name'] = $list->getName();
        }

        return new JsonResponse($result);
    }

    /* edit list details through ajax */
    public function edit(Request $request)
    {
        $data = $request->request->all();
        $result['status'] = "fail";

        if ($data) {
            $update = $this->listsRepository->update($data);

            if ($update){
                $result['status'] = "success";
                $result['name'] = $update->getName();
                $result['id'] = $update->getId();
            }
        }
        return new JsonResponse($result);
    }

    /* add new item into existing list*/
    public function newListItem(Request $request): JsonResponse
    {
        $data = $request->request->all();
        $form = $this->listItemForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /* call repository function to create new item into list into database  */
            $addItem = $this->listItemRepository->create($form);

            if ($addItem == "false"){
                $result['status'] = "fail";
            }else{
                $result['status'] = "success";
                $result['item']['id'] = $addItem->getId();
                $result['item']['name'] = $addItem->getItemName();
                $result['item']['place'] = $addItem->getPlacedAt();
                $result['item']['color'] = $addItem->getColor();
                $result['item']['list'] = $addItem->getList()->getId();
                $result['item']['deleteUrl'] = $this->generateUrl("list_items_delete", ['id'=>base64_encode($addItem->getId())] );
            }
        }else{
            $result['status'] = "fail";    
        }
        
        return new JsonResponse($result);
    }

    /* to delete list and its items */
    public function delete(Request $request, $id): JsonResponse
    {
        $result['status'] = "fail";        
        $list = $this->listsRepository->find(base64_decode($id));
            
        $delete = $this->listsRepository->deleteList($list);
        if ($delete == true) {
            $result['status'] = "success";
        }

        return new JsonResponse($result);
    }

    /* to delete item from list */
    public function deleteListItem(Request $request, $id): JsonResponse
    {
        $result['status'] = 'fail';
        $listItem = $this->listItemRepository->find(base64_decode($id));
        $id = $listItem->getId();   
        $delete = $this->listItemRepository->deleteItem($listItem);
        if ($delete == true) {
            $result['status'] = "success";
        }
        return new JsonResponse($result);   
    }
}
