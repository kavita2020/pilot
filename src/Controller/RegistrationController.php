<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Security\EmailVerifier;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegistrationController extends AbstractController
{
    private $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, ValidatorInterface $validator): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('lists_index');
        }
        
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
        $data = $request->request->all();
        $errors = [];

        if ($form->isSubmitted() ) {
            $first_pass = $data['registration_form']['password']['first'];
            $second_pass = $data['registration_form']['password']['second'];
            if ($first_pass == $second_pass){
                
                // encode the password
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $second_pass
                    )
                );
                $user->setFirstTimeLogin(true);
                if ($form->isValid()) {
                
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($user);
                    $entityManager->flush();

                    // generate a signed url and email it to the user
                    $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user,
                        (new TemplatedEmail())
                            ->from(new Address('amitkushwahaaa7@gmail.com', 'Pilot Mail'))
                            ->to($user->getEmail())
                            ->subject('Please Confirm your Email')
                            ->htmlTemplate('registration/confirmation_email.html.twig')
                    );
                    // do anything else you need here, like send an email

                    $this->addFlash('success', 'A Mail is send to you your email-id. Please confirm your email');
                    return $this->redirectToRoute('app_register');
                }

                $errors = $validator->validate($user);
                
            }else{
                $this->addFlash('error','Enter same password');
                return $this->redirectToRoute('app_register');    
            }
        }

        return $this->render('registration/register.html.twig', [
            'errors' => $errors,
            'registrationForm' => $form->createView(),
        ]);
    }

    public function verifyUserEmail(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $this->getUser());
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $exception->getReason());

            return $this->redirectToRoute('app_register');
        }

        // @TODO Change the redirect on success and handle or remove the flash message in your templates
        $this->addFlash('success', 'Your email address has been verified.');

        // return $this->redirectToRoute('app_login');
        return $this->redirectToRoute('lists_index');
    }
}
