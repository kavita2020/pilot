<?php

namespace App\Repository;

use App\Entity\ListItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\ListsRepository;
/**
 * @method ListItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListItem[]    findAll()
 * @method ListItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListItemRepository extends ServiceEntityRepository
{
    private $listsRepository;
    public function __construct(ManagerRegistry $registry, ListsRepository $listsRepository)
    {
        parent::__construct($registry, ListItem::class);
        $this->listsRepository = $listsRepository;
    }

    public function create($form)
    {
        $list_id = base64_decode($form->get('list_id')->getData());
        $list = $this->listsRepository->find($list_id);

        $item = new ListItem();
        $item->setItemName($form->get('item_name')->getData());
        $item->setList($list);
        $item->setPlacedAt($form->get('placed_at')->getData());
        $item->setColor($form->get('color')->getData());

        $this->_em->persist($item);
        $this->_em->flush();

        if($item){
            return $item;
        }else{
            return false;
        }

    }

    public function deleteItem($item)
    {
        $this->_em->remove($item);
        $this->_em->flush();

        return true;
    }
    // /**
    //  * @return ListItem[] Returns an array of ListItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ListItem
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
