<?php

namespace App\Repository;

use App\Entity\Lists;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\UserRepository;

/**
 * @method Lists|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lists|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lists[]    findAll()
 * @method Lists[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListsRepository extends ServiceEntityRepository
{
    private $userRepository;
    public function __construct(ManagerRegistry $registry, UserRepository $userRepository)
    {
        parent::__construct($registry, Lists::class);
        $this->userRepository = $userRepository;
    }

    public function create($data)
    {   
        $user_id = base64_decode($data->get('user_id')->getData());
        $user = $this->userRepository->find($user_id);
        
        $list = new Lists();
        $list->setName($data->get('name')->getData());
        $list->setUser($user);

        $this->_em->persist($list);
        $this->_em->flush();

        if ($list) {
            return $list;
        }else{
            return 'fail';
        }
    }

    public function deleteList($list)
    {
        $this->_em->remove($list);
        $this->_em->flush();

        return true;        
    }

    public function update($data)
    {
        $id = base64_decode($data['list_id']);
        $list = Self::find($id);
        
        if ($list) {
            $list->setName($data['name']);
            $this->_em->persist($list);
            $this->_em->flush();
            return $list;
        }
        return false;
    }

    // /**
    //  * @return Lists[] Returns an array of Lists objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Lists
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
