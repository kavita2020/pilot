/* form submit to add new list */
$(document).on('submit', 'form#listForm', function(event){
	event.preventDefault();
	var form = $(this);
	var url = $(this).attr('action');
	var formData = $(this).serialize();

	$.ajax({
		url: url,
		type: 'POST',
		data: formData,
		success: function(data){

			if (data.status == "success"){
				$('.noList').remove();
				appendListDiv(data.list);
				$('form#listForm')[0].reset();
				$('#addListModal').modal('hide');
			}

		},
	})
});

/* append new list after inserting in database */
function appendListDiv(list) {
	$('.listsDiv').prepend('<div class="col-lg-4 listFor"> <div class="card shadow mb-4"> <div class="d-block card-header py-3"> <a href="#collapseCardExample'+ list.id +'" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample'+ list.id +'"> <h6 class="m-0 font-weight-bold text-primary">'+ list.name +'</h6> </a> <form id="deleteListForm" action="'+ list.url +'"> <input type="hidden" name="_method" value="DELETE"> <button class="list_delete_icon" title="Delete list"><i class="fa fa-times "></i></button> </form> </div> <div class="collapse show" id="collapseCardExample'+ list.id +'"> <div class="card-body"> <div class="item_add_btn"> <button class="btn btn-info add_item_btn" thisList="'+ list.encode_id +'"> <i class="fa fa-plus"> </i> Add item </button> </div> <div class="itemDiv thisList'+ list.id +'"></div> </div> </div> </div> </div>'); 
}

/* form submit to delete list */
$(document).on('submit', 'form#deleteListForm', function(event){
	event.preventDefault();
	if( confirm('Are you sure you want to remove this list?')) {
		var form = $(this);
		var url = form.attr('action');
		var formData = form.serialize();
		$.ajax({
			url: url,
			type: 'DELETE',
			data: formData,
			success: function(data){
				if (data.status == "success") {
					form.closest('.listFor').remove();
				}		
			}
		});
	}
})

/* open model to add new item into list */
$(document).on('click', '.add_item_btn', function(){
	var listId = $(this).attr('thisList');
	$('input[getListid]').val(listId);
	$('#addItemModal').modal('show');
});

/* form submit to add new item into div */
$(document).on('submit', 'form#listItemForm', function(event){
	event.preventDefault();
	$('.addItemError').remove();
	var form = $(this);
	var formData = form.serialize();
	var url = form.attr('action');
	$.ajax({
		url: url,
		type: 'POST',
		data: formData,
		success: function(data){
			if (data.status == "success") {
				var item = data.item;
				var item_position = item.place;
				
				if (item_position == "top") {
					$('.thisList'+item.list).prepend('<div class="px-3 py-3 bg_list_item thisItem" style="color:'+ item.color +';"><form id="deleteItemForm" action="'+ item.deleteUrl+ '"> <input type="hidden" name="_method" value="DELETE"> <button class="itm_del_icon" title="Delete item"><i class="fa fa-times"></i></button> </form>'+ item.name +'</div>'); 
				}else{
					$('.thisList'+item.list).append('<div class="px-3 py-3 bg_list_item thisItem" style="color:'+ item.color +';"><form id="deleteItemForm" action="'+ item.deleteUrl+ '"> <input type="hidden" name="_method" value="DELETE"> <button class="itm_del_icon" title="Delete item"><i class="fa fa-times"></i></button> </form>'+ item.name +'</div>');
				}
				
				$('form#listItemForm')[0].reset();
				$('#addItemModal').modal('hide');
			}else{
				$('#addItemModal .modal-body').prepend('<div class="alert alert-danger addItemError" role="alert"> Something went wrong. Please try again. </div>');
			}
		}
	})
});

/* form submit to delete item from list */
$(document).on('submit', 'form#deleteItemForm',function(e){
	e.preventDefault();
	var form = $(this);
	var url = form.attr('action');
	var formData = form.serialize();
	$.ajax({
		url: url,
		type: 'DELETE',
		data: {formData, _token: "{{ csrf_token('delete') }}"},
		success: function(data){
			if (data.status == "success") {
				form.closest('.thisItem').remove();
			}		
		}
	});
})

/* get list detail and open edit modal form */
$(document).on('click', '.list_edit_icon', function(){
	var url = $(this).attr('hitUrl');

	$.get( url, function( data ) {

	  	if (data.status == "success") {
	  		$('#get_list_name').val(data.name);
	  		$('#get_list_id').val(data.id);
	  		$('#editListModal').modal('show');
	  	}

	});
});

/* form submit to edit item name  */
$(document).on('submit','form#editListForm', function(e){
	e.preventDefault();
	var formData = $(this).serialize();
	var url = $(this).attr('action');

	$.post(url, formData, function(data){
		if (data.status == "success"){
			$(".listNameHead"+data.id).text("");
			$(".listNameHead"+data.id).text(data.name);
	  		$('#editListModal').modal('hide');
		}
	});
});